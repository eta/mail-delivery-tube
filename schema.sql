CREATE TABLE mails (
  id INTEGER PRIMARY KEY,
  parent_id INTEGER REFERENCES mails ON DELETE CASCADE
);

CREATE TABLE header_keys (
  id INTEGER PRIMARY KEY,
  string VARCHAR NOT NULL
);

CREATE TABLE mail_headers (
  id INTEGER PRIMARY KEY,
  mail_id INTEGER NOT NULL REFERENCES mails ON DELETE CASCADE,
  key_id INTEGER NOT NULL REFERENCES header_keys ON DELETE RESTRICT,
  val VARCHAR NOT NULL
);

CREATE TABLE mail_bodies (
  id INTEGER PRIMARY KEY,
  mail_id INTEGER UNIQUE NOT NULL REFERENCES mails ON DELETE CASCADE,
  content_type VARCHAR NOT NULL,
  binary BLOB,
  text VARCHAR,
  -- binary XOR text must be true
  CHECK((binary IS NULL) != (text IS NULL))
);

CREATE TABLE tag_names (
  id INTEGER PRIMARY KEY,
  string VARCHAR NOT NULL
);

CREATE TABLE mail_tags (
  id INTEGER PRIMARY KEY,
  mail_id INTEGER NOT NULL REFERENCES mails ON DELETE CASCADE,
  tag_id INTEGER NOT NULL REFERENCES tag_names ON DELETE RESTRICT
);

--CREATE VIRTUAL TABLE mail_headers_val_fts USING fts4(content="mail_headers", val TEXT);
--CREATE VIRTUAL TABLE header_keys_fts USING fts4(content="header_keys", string TEXT);
--CREATE VIRTUAL TABLE mail_bodies_fts USING fts4(content="mail_bodies", text TEXT);
