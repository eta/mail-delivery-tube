(ql:quickload '(queues.simple-queue flexi-streams babel qbase64 trivial-gray-streams bordeaux-threads sqlite))

(defpackage :mail-delivery-tube
  (:nicknames :mdt)
  (:use :cl))

(in-package :mdt)

(defun read-header-name (stream)
  "Read a header name from STREAM."
  (coerce
   (loop
     with char
     do (setf char (read-char stream))
     while (not (char= char #\:))
     collect char)
   'string))

(defun wspp (char)
  "Returns T if CHAR is whitespace."
  (member char '(#\Return #\Linefeed #\Space #\Tab)))

(defun read-header-value (stream)
  (let ((ret (make-string-output-stream))
        (char))
    (tagbody
     start
       (unless (setf char (read-char stream nil nil))
         (go end))
       (when (char= char #\return)
         (read-char stream) ; eat the LF
         ;; if the first char on the next line isn't a WSP,
         ;; return (it's a new header)
         (unless (member (peek-char nil stream) '(#\Space #\Tab))
           (go end))
         (go start))
       (write-char char ret)
       (go start)
     end
       (return-from read-header-value
         (get-output-stream-string ret)))))

(defun read-header (stream)
  "Read a mail header from STREAM, returning a cons pair."
  (cons (read-header-name stream)
        (progn
          (read-char stream) ; eat the space
          (read-header-value stream))))

(defun read-headers (stream)
  "Read all mail headers from STREAM, returning a list of cons pairs."
  (wschomp stream)
  (loop
    while (not (wspp (peek-char nil stream)))
    collect (read-header stream)))

(defun read-until (stream predicate)
  "Reads from STREAM until CHAR fulfils PREDICATE, at which point CHAR is discarded and the stream is returned."
  (with-output-to-string (s)
    (loop
      with c
      while (setf c (read-char stream nil nil))
      while (not (funcall predicate c))
      do (write-char c s))))

(defun read-until-char (stream char)
  "Calls READ-UNTIL with PREDICATE testing for equality with CHAR."
  (read-until stream (lambda (x) (char= x char))))

(defun read-quoted-string (stream)
  "Read a quoted-string from STREAM."
  (when (char= #\" (peek-char nil stream))
    (with-output-to-string (out)
      (read-char stream) ; eat opening quote
      (let ((char))
        (tagbody
         start
           (unless (setf char (read-char stream nil nil))
             (go end))
           (when (char= char #\")
             (go end))
           (when (char= char #\\)
             (write-char (read-char stream) out)
             (go start))
           (write-char char out)
           (go start)
         end)))))

(defun wschomp (stream)
  (peek-char t stream nil nil))

(defun parse-content-type-header (stream)
  "Parse the Content-Type header value provided in STREAM."
  (let ((type (read-until-char stream #\/))
        (subtype (read-until-char stream #\;))
        (parameters (loop
                      ;; If there's a ; left over from the last iteration,
                      ;; chomp it
                      do (when (eql (peek-char nil stream nil nil) #\;)
                           (read-char stream))
                      ;; Chomp any whitespace
                      do (wschomp stream)
                      ;; Check for EOF
                      while (peek-char nil stream nil nil)
                      append (list (intern
                                    (string-upcase
                                     (read-until-char stream #\=)) :keyword)
                                   (or
                                    (read-quoted-string stream)
                                    (read-until
                                     stream
                                     (lambda (x)
                                       (member x '(#\Space #\Tab #\;)))))))))
    `(,type ,subtype ,@parameters)))

(defclass body-stream (trivial-gray-streams:fundamental-binary-input-stream)
  ((boundary
    :initarg :boundary
    :accessor boundary)
   (pointer
    :initform 0
    :accessor pointer)
   (spare
    :initform (queues:make-queue :simple-queue)
    :accessor spare)
   (inner
    :initarg :inner
    :accessor inner)))

(defun reset-body-stream (bs)
  (setf (pointer bs) 0)
  (setf (spare bs) (queues:make-queue :simple-queue)))

(defmethod trivial-gray-streams:stream-read-byte ((bs body-stream))
  (block nil
    (tagbody
       (when (> (queues:qsize (spare bs)) 0)
         (return (queues:qpop (spare bs))))
     read
       (when (eql (pointer bs) (length (boundary bs)))
         (return :eof))
       (let ((byte (handler-case
                       (read-byte (inner bs))
                     (end-of-file ()
                       (return :eof))))
             (comparison (elt (boundary bs) (pointer bs))))
         (when (eql byte comparison)
           (queues:qpush (spare bs) byte)
           (incf (pointer bs))
           (go read))
         (setf (pointer bs) 0)
         (when (> (queues:qsize (spare bs)) 0)
           (queues:qpush (spare bs) byte)
           (return (queues:qpop (spare bs))))
         (return byte)))))

(defmethod trivial-gray-streams:stream-read-sequence ((bs body-stream) sequence start end &key &allow-other-keys)
  (let ((ret 0))
    (loop
      for i from (or start 0) below (or end (1- (length sequence)))
      do (setf ret i)
      do (let ((byte (trivial-gray-streams:stream-read-byte bs)))
           (when (eql byte :eof)
             (return-from trivial-gray-streams:stream-read-sequence ret))
           (setf (elt sequence i) (trivial-gray-streams:stream-read-byte bs))))
    (incf ret)))

(defun test-body-stream ()
  (let* ((orig (flexi-streams:make-in-memory-input-stream
                #(1 2 3 4 5 6 2 3 6 7 1 2 3 6 7 8 9 10)))
         (bs (make-instance 'body-stream
                            :boundary #(6 7 8)
                            :inner orig))
         (read (loop
                 with byte
                 while (setf byte (read-byte bs nil nil))
                 collect byte)))
    (format t "~A~%" read)))

(defclass mail-body ()
  ((transfer-encoding
    :initarg :transfer-encoding
    :reader transfer-encoding)
   (content-type
    :initarg :content-type
    :reader content-type)))

(defclass text-mail-body (mail-body)
  ((text
    :initarg :text
    :reader text)))

(defclass binary-mail-body (mail-body)
  ((binary
    :initarg :binary
    :reader binary)))

(defclass simple-asciifying-stream (trivial-gray-streams:fundamental-character-input-stream)
  ((inner
    :initarg :inner
    :accessor inner)))

(defmethod trivial-gray-streams:stream-read-char ((sas simple-asciifying-stream))
  ;; Turns out some consumers are really naughty and try to read from streams
  ;; after EOF (the base64 stream thing does this).
  (handler-case
      (if (inner sas)
          (code-char (read-byte (inner sas)))
          :eof)
    (end-of-file ()
      (progn
        (setf (inner sas) nil)
        :eof))))

(defclass quoted-printable-stream (trivial-gray-streams:fundamental-binary-input-stream)
  ((inner
    :initarg :inner
    :accessor inner)))

(defmethod trivial-gray-streams:stream-read-byte ((qps quoted-printable-stream))
  (block nil
    (handler-case
        (tagbody
         start
           (let ((byte (read-byte (inner qps))))
             (when (eql byte #.(char-code #\=))
               (let ((next-two-bytes
                       (coerce
                        (list
                         (code-char (read-byte (inner qps)))
                         (code-char (read-byte (inner qps))))
                        'string)))
                 (when (eql (elt next-two-bytes 0) #\Return) ; soft line break
                   (go start))
                 (return (parse-integer next-two-bytes
                                        :radix 16))))
             (return byte)))
      (end-of-file () (return :eof)))))

(defun test-qp-stream ()
  (let* ((teststring (concatenate 'string
                                  "Car ils se font vite p=C3=A9dagogues ="
                                  (coerce '(#\Return #\Linefeed) 'string)
                                  "soft linebreak"
                                  (coerce '(#\Return #\Linefeed) 'string)
                                  "hard linebreak"))
         (testbytes (babel:string-to-octets teststring))
         (flexstream (flexi-streams:make-in-memory-input-stream testbytes))
         (qps (make-instance 'quoted-printable-stream
                             :inner flexstream))
         (readstring (flexi-streams:make-flexi-stream qps
                                                      :external-format :utf-8)))
    (with-output-to-string (out)
      (loop
        with char
        while (setf char (read-char readstring nil nil))
        do (write-char char out)))))

(defun make-mail-body-stream (original transfer-encoding)
  (cond
    ((string-equal transfer-encoding "quoted-printable")
     (make-instance 'quoted-printable-stream
                    :inner original))
    ((string-equal transfer-encoding "base64")
     (make-instance 'qbase64:decode-stream
                    :underlying-stream (make-instance 'simple-asciifying-stream
                                                      :inner original)))
    (t original)))

(defun read-text-mail-body (strm content-type transfer-encoding charset)
  (let* ((flexstream (flexi-streams:make-flexi-stream
                      strm
                      :external-format (intern
                                        (or charset "utf8")
                                        :keyword)))
         (body
           (with-output-to-string (out)
             (loop
               with char
               while (setf char (read-char flexstream nil nil))
               do (unless (eql char #\Return) ; CRLF -> LF
                    (write-char char out))))))
    (make-instance 'text-mail-body
                   :content-type content-type
                   :transfer-encoding transfer-encoding
                   :text body)))

(defun read-binary-mail-body (strm content-type transfer-encoding)
  (make-instance 'binary-mail-body
                 :content-type content-type
                 :transfer-encoding transfer-encoding
                 :binary
                 (flexi-streams:with-output-to-sequence (out)
                   (loop
                     with byte
                     while (setf byte (read-byte strm nil nil))
                     do (write-byte byte out)))))

(defun read-mail (strm)
  (let* ((flexi-streams:*substitution-char* #.(code-char #xFFFD))
         (headers-stream (flexi-streams:make-flexi-stream strm
                                                          :external-format :us-ascii))
         (headers (read-headers headers-stream))
         (content-type-header (or
                               (cdr (assoc "Content-Type" headers
                                           :test #'string-equal))
                               "text/plain"))
         (transfer-encoding-header (cdr (assoc "Content-Transfer-Encoding" headers
                                               :test #'string-equal))))
    (destructuring-bind (type subtype &key boundary charset &allow-other-keys)
        (parse-content-type-header (make-string-input-stream content-type-header))
      (declare (ignore subtype))
      (when (string-equal type "multipart")
        (assert boundary (boundary) "No boundary provided for multipart message")
        (let* ((boundary (concatenate 'string "--" boundary))
               (boundary-bytes (babel:string-to-octets boundary))
               (body-stream (make-instance 'body-stream
                                           :inner strm
                                           :boundary boundary-bytes))
               (mails))
          ;; Read up until the first boundary.
          (loop while (read-byte body-stream nil nil))
          (handler-case
              (loop
                do (reset-body-stream body-stream)
                do (push (read-mail body-stream) mails))
            (end-of-file ()
              (return-from read-mail (cons (cons headers nil) mails))))))
      (read-byte strm nil nil) ; whitespace
      (let* ((body-stream (make-mail-body-stream strm transfer-encoding-header))
             (body (if (string-equal type "text")
                       (read-text-mail-body
                        body-stream
                        content-type-header
                        transfer-encoding-header
                        charset)
                       (read-binary-mail-body
                        body-stream
                        content-type-header
                        transfer-encoding-header))))
        (cons headers body)))))

(defun new-mail-id (&optional parent-id)
  "Mint a new fresh mail ID, with an optional parent."
  (tq::with-prepared-statements
      ((insert "INSERT INTO mails (parent_id) VALUES (?)"))
    (sqlite:bind-parameter insert 1 parent-id)
    (sqlite:step-statement insert)
    (sqlite:last-insert-rowid tq::*db*)))

(defun intern-header-key (key)
  "Intern the header KEY in the database and return its key ID."
  (let ((key (string-downcase key)))
    (tq::with-prepared-statements
        ((get "SELECT id FROM header_keys WHERE string = ?")
         (insert "INSERT INTO header_keys (string) VALUES (?)"))
      (sqlite:bind-parameter get 1 key)
      (if (sqlite:step-statement get)
          (car (tq::column-values get))
          (progn
            (sqlite:bind-parameter insert 1 key)
            (sqlite:step-statement insert)
            (sqlite:last-insert-rowid tq::*db*))))))

(defun intern-tag-name (tag)
  "Intern the TAG in the database and return its ID."
  (tq::with-prepared-statements
      ((get "SELECT id FROM tag_names WHERE string = ?")
       (insert "INSERT INTO tag_names (string) VALUES (?)"))
    (sqlite:bind-parameter get 1 tag)
    (if (sqlite:step-statement get)
        (car (tq::column-values get))
        (progn
          (sqlite:bind-parameter insert 1 tag)
          (sqlite:step-statement insert)
          (sqlite:last-insert-rowid tq::*db*)))))

(defun tag-mail (mail-id tag)
  "Tag the MAIL-ID with TAG."
  (tq::with-prepared-statements
      ((insert "INSERT INTO mail_tags (mail_id, tag_id) VALUES (?, ?)"))
    (let ((tagid (intern-tag-name tag)))
      (tq::bind-parameters insert mail-id tagid)
      (sqlite:step-statement insert))))

(defun insert-header (mail-id key value)
  "Insert the KEY: VALUE header for MAIL-ID."
  (tq::with-prepared-statements
      ((insert "INSERT INTO mail_headers (mail_id, key_id, val) VALUES (?, ?, ?)"))
    (let ((keyid (intern-header-key key)))
      (tq::bind-parameters insert mail-id keyid value)
      (sqlite:step-statement insert)
      (sqlite:last-insert-rowid insert))))

(defun insert-headers (mail-id headers)
  "Insert a list of mail HEADERS into the db for MAIL-ID."
  (loop
    for header in headers
    do (insert-header mail-id (car header) (cdr header))))

(defun insert-body (mail-id body)
  "Insert a BODY, either a TEXT-MAIL-BODY or a BINARY-MAIL-BODY, into the DB."
  (tq::with-prepared-statements
      ((insert-text "INSERT INTO mail_bodies (mail_id, text, content_type) VALUES (?, ?, ?)")
       (insert-binary "INSERT INTO mail_bodies (mail_id, binary, content_type) VALUES (?, ?, ?)"))
    (multiple-value-bind (statement value)
        (etypecase body
          (text-mail-body (values insert-text (text body)))
          (binary-mail-body (values insert-binary (subseq (binary body) 0))))
      (let ((content-type (content-type body)))
        (tq::bind-parameters statement mail-id value content-type))
      (sqlite:step-statement statement))))

(defun insert-mail-into-db (mail &optional parent-id)
  "Insert a MAIL into the database."
  (let ((mail-id (new-mail-id parent-id)))
    (insert-headers mail-id (car mail))
    (when (cdr mail)
      (insert-body mail-id (cdr mail)))
    mail-id))

(defun insert-mails-into-db (mails &optional parent-id)
  "Insert MAILS (the output of READ-MAIL) into the database. Returns the parent mail ID."
  (if (typep (cdr mails) 'mail-body)
      ;; Just a single non-multipart email
      (insert-mail-into-db mails parent-id)
      (destructuring-bind (first &rest rest) mails
        (let ((parent-id (insert-mail-into-db first parent-id)))
          (loop
            for mail in rest
            do (insert-mails-into-db mail parent-id))
          parent-id))))

(defun insert-file-into-db (pathname)
  "Read the mail at PATHNAME and insert it into the database."
  (with-open-file (strm pathname
                        :element-type '(unsigned-byte 8))
    (with-simple-restart (continue "Give up on file ~A." pathname)
      (tq::with-transaction
        (let* ((bundle (read-mail strm))
               (mail-id (insert-mails-into-db bundle)))
          (format *debug-io* "~&~A inserted as #~A~%" pathname mail-id)
          mail-id)))))

(defun insert-maildir (pathname)
  "Insert the entire maildir at PATHNAME into the database."
  (with-simple-restart (stop "Stop insertion.")
    (loop
      for dir in (directory (format nil "~A/*/" pathname))
      do (handler-bind
             ((flexi-streams:external-format-error
                (lambda (c)
                  (format t "*** ~A *** ~%" c)
                  (invoke-restart 'continue))))
           (let ((folder-name (car (last (pathname-directory dir)))))
             (format t "~&* Dealing with folder ~A~%" folder-name)
             (loop
               for mail in (uiop:directory-files (format nil "~A/cur/" dir))
               do (let ((mail-id (time (insert-file-into-db mail))))
                    (when mail-id
                      (tag-mail mail-id folder-name)
                      (tag-mail mail-id "seen"))))
             (loop
               for mail in (uiop:directory-files (format nil "~A/new/" dir))
               do (let ((mail-id (insert-file-into-db mail)))
                    (when mail-id
                      (tag-mail mail-id folder-name)
                      (tag-mail mail-id "unread")))))))))

(defun fuzz-test (corpus-dir)
  (loop
    for file in (uiop:directory-files corpus-dir)
    do (progn
         (format t "~&Testing ~A~%" file)
         (with-open-file (strm file :element-type '(unsigned-byte 8))
           (with-simple-restart (continue "Give up on file ~A." file)
             (handler-case
                 (progn
                   (let ((mail (read-mail strm)))
                     (format t "~&OK ~A~%" mail)))
               (flexi-streams:external-format-encoding-error (e) (format t "~&format error ~A~%" e))
               (end-of-file () (format t "~&Result: EOF~%"))))))))

(setf (cl-who:html-mode) :html5)

(defvar *title* "mail-delivery-tube")
(defparameter *iframe-magic-js* "
window.resize_frame = function resize_frame(iFrame) {
    //iFrame.width  = iFrame.contentWindow.document.body.scrollWidth;
    var height = iFrame.contentWindow.document.body.scrollHeight;
    console.log(\"height: \" + height);
    iFrame.height = iFrame.contentWindow.document.body.scrollHeight;
}

function resize_iframes() {
    var iframes = document.querySelectorAll('iframe');
    console.log(\"resizing \" + iframes.length + \" iframes\");
    for( var i = 0; i < iframes.length; i++) {
        resizeIFrameToFitContent( iframes[i] );
    }
}

//window.addEventListener('DOMContentLoaded', resize_iframes);
")
(defparameter *styles* "
.header {
    display: block;
    background: #ececec;
    padding-left: 8rem;
}
.header-name {
    display: inline-block;
    padding: 3px;
    margin-right: 0.5rem;
    width: 7rem;
    font-family: 'Fantasque Sans Mono', monospace;
    text-align: right;
    background: lightgray;
    padding-right: 0.5rem !important;
    text-transform: capitalize;
    margin-left: -8rem;
}
.mail-attachment {
    padding: 8px;
    display: block;
    border: 3px dashed lightblue;
    border-radius: 5px;
    margin: 3px;
    margin-top: 10px;
}
.mail-headers-extra-summary {
    background: #ececec;
    padding-left: 8rem;
    font-size: 10pt;
    font-family: 'Fantasque Sans Mono', monospace;
}
.mail-name {
    font-size: 10pt;
    color: darkred;
    font-weight: bold;
    text-align: right;
}
.mail-body-iframe {
    width: 100%;
    border: none;
}
.mail-name-extra {
    display: none;
    padding-left: 5px;
    padding-right: 5px;
    font-family: 'Fantasque Sans Mono', monospace;
    font-weight: normal;
}
.mail-name:hover .mail-name-extra {
    display: inline-block;
}
")

;; Copied from TVL's panettone, with thanks to grfn
(defmacro render (() &body body)
  `(who:with-html-output-to-string (*standard-output* nil :prologue t)
     (:html
      :lang "en"
      (:head
       (:title (who:esc *title*))
       (:link :rel "stylesheet" :type "text/css" :href "//theta.eu.org/assets/webfonts/stylesheet.css")
       (:link :rel "stylesheet" :type "text/css" :href "//theta.eu.org/css/main.css")
       (:style (who:str *styles*))
       (:script (who:str *iframe-magic-js*))
       (:meta :name "viewport"
              :content "width=device-width,initial-scale=1"))
      (:header
       :class "site-header"
       (:div
        :class "wrapper"
        (:nav
         :class "site-nav"
         (:div
          :class "trigger"
          "The Mail Delivery Tube"))))
      (:div
       :class "page-content"
       (:div
        :class "wrapper"
        ,@body)))))

(defun count-mails ()
  "Count the total number of mails in the database."
  (tq::with-prepared-statements
      ((count "SELECT COUNT(*) FROM mails WHERE parent_id IS NULL"))
    (sqlite:step-statement count)
    (sqlite:statement-column-value count 0)))

(hunchentoot:define-easy-handler (body :uri "/body") ((id :parameter-type 'integer))
  (unless id
    (setf (hunchentoot:return-code*) hunchentoot:+http-bad-request+)
    (hunchentoot:abort-request-handler "an id must be provided"))
  (tq::with-prepared-statements
      ((select "SELECT mail_id, content_type, binary, text FROM mail_bodies WHERE id = ?"))
    (sqlite:bind-parameter select 1 id)
    (unless (sqlite:step-statement select)
      (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
      (hunchentoot:abort-request-handler "what file is that"))
    (tq::with-bound-columns (mail-id content-type binary text) select
      (setf (hunchentoot:content-type*) content-type)
      (setf (hunchentoot:header-out "X-MDT-For-Mail") (princ-to-string mail-id))
      (setf (hunchentoot:header-out "Access-Control-Allow-Origin") "*")
      (setf (hunchentoot:header-out "Content-Security-Policy") "sandbox; default-src 'none'")
      (or binary text))))

(defun get-mail-headers (mail-id)
  "Gets the mail headers for MAIL-ID as an alist."
  (tq::with-prepared-statements
      ((select "SELECT header_keys.string, mail_headers.val FROM mail_headers, header_keys WHERE mail_headers.mail_id = ? AND mail_headers.key_id = header_keys.id"))
    (sqlite:bind-parameter select 1 mail-id)
    (loop
      while (sqlite:step-statement select)
      collect (cons
               (sqlite:statement-column-value select 0)
               (sqlite:statement-column-value select 1)))))

(defparameter *interesting-headers* '("from" "to" "x-mailer" "reply-to"))

(defun filter-mail-headers (headers &optional (allowed-keys *interesting-headers*))
  (remove-if (lambda (cp) (not (member (car cp) allowed-keys :test #'string-equal)))
             (copy-seq headers)))

(defun render-headers (alist &optional (out *standard-output*))
  (who:with-html-output (out)
    (loop
      for (name . value) in alist
      do (who:htm
          (:div
           :class "header"
           (:span
            :class "header-name"
            (who:esc name))
           (:span
            :class "header-value"
            (who:esc value)))))))

(defun render-body-iframe (body-id &optional (out *standard-output*))
  (who:with-html-output (out)
    (:iframe
     ;; https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe
     :sandbox ""
     :scrolling "no"
     :height "1024px"
     :onload "resize_frame(this)"
     :class "mail-body-iframe"
     :src (format nil "/body?id=~A" body-id))))

(defun get-child-mails (mail-id)
  "Get the child mail IDs (if any) for MAIL-ID."
  (tq::with-prepared-statements
      ((select "SELECT id FROM mails WHERE parent_id = ?"))
    (sqlite:bind-parameter select 1 mail-id)
    (loop
      while (sqlite:step-statement select)
      collect (sqlite:statement-column-value select 0))))

(defun get-body-id (mail-id)
  "Get the body ID (if any) for MAIL-ID."
  (tq::with-prepared-statements
      ((select "SELECT id FROM mail_bodies WHERE mail_id = ?"))
    (sqlite:bind-parameter select 1 mail-id)
    (when (sqlite:step-statement select)
      (sqlite:statement-column-value select 0))))

(defun render-mail (mail-id &key (out *standard-output*) (open t))
  (let* ((mail-headers (get-mail-headers mail-id))
         (filtered-headers (filter-mail-headers mail-headers))
         (content-type (cdr (assoc "content-type" mail-headers :test #'string-equal)))
         (content-type (when content-type
                         (parse-content-type-header (make-string-input-stream content-type))))
         (subject (cdr (assoc "subject" mail-headers :test #'string-equal)))
         (content-disposition (cdr (assoc "content-disposition" mail-headers :test #'string-equal)))
         (body-id (get-body-id mail-id)))
    (who:with-html-output (out)
      (:details
       :class "mail"
       :open "true"
       (:h2
        (who:esc subject))
       (:summary
        :class "mail-name"
        (if content-type
            (destructuring-bind (type subtype &rest rest) content-type
              (who:htm
               (when rest
                 (who:htm
                  (:span
                   :class "mail-name-extra"
                   (who:esc (format nil "~A" rest)))))
               (who:fmt "#~A: ~A/~A" mail-id type subtype)))
            (who:fmt "#~A (no content type)" mail-id)))
       (:div
        :class "mail-inner"
        (:div
         :class "mail-headers"
         (render-headers filtered-headers)
         (:details
          :class "mail-headers-extra"
          (:summary
           :class "mail-headers-extra-summary"
           "extra headers")
          (render-headers mail-headers)))
        (when body-id
          (if (and content-type (uiop:string-prefix-p "attachment" content-disposition))
              (who:htm
               (:a
                :class "mail-attachment"
                :href (format nil "/body?id=~A" body-id)
                (destructuring-bind (type subtype &key name &allow-other-keys) content-type
                  (if name
                      (who:esc
                       (format nil "Download '~A' (~A/~A)" name type subtype))
                      (who:esc
                       (format nil "Download attachment (~A/~A)" type subtype))))))
              (who:htm
               (:div
                :class "mail-body"
                (render-body-iframe body-id)))))
        (loop
          for mail-id in (get-child-mails mail-id)
          do (render-mail mail-id :open nil)))))))

(hunchentoot:define-easy-handler (mail :uri "/mail") ((id :parameter-type 'integer))
  (unless id
    (setf (hunchentoot:return-code*) hunchentoot:+http-bad-request+)
    (hunchentoot:abort-request-handler "an id must be provided"))
  (render ()
    (render-mail id)))

(hunchentoot:define-easy-handler (root :uri "/") ()
  (render ()
    (:h1
     "mail delivery tube")))

(defun main ()
  (tq::connect-database)
  (hunchentoot:start
   (make-instance 'hunchentoot:easy-acceptor
                  :port 4242)))
