(defpackage :mail-delivery-tube.tests
  (:nicknames :mdt.tests)
  (:use :mdt :cl :fiveam))

(in-package :mdt.tests)

(def-suite mdt-tests
  :description "testing the mail delivery tube")

(in-suite mdt-tests)

(defun mdt-test-all ()
  (run! 'mdt-tests))
